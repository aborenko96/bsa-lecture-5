<?php

namespace App\Policies;

use App\Entity\Product;
use App\Entity\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function edit(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $this->isAdmin($user);
    }

    public function delete(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $this->isAdmin($user);
    }

    private function isUsersProduct(User $user, Product $product)
    {
        return $product->user_id === $user->id;
    }

    private function isAdmin(User $user)
    {
        return (bool)$user->is_admin;
    }
}
