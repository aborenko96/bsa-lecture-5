<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
