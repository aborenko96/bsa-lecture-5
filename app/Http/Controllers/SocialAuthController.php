<?php

namespace App\Http\Controllers;

use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $redirectPath = '/products';

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $socialUser = Socialite::driver('facebook')->stateless()->user();

        $user = User::where(['social_id' => $socialUser->id])->first();

        if (is_null($user)) {
            $user = User::create([
                'name' => $socialUser->name,
                'social_id' => $socialUser->id,
                'email' => '',
                'password' => ''
            ]);
        }

        Auth::login($user);

        return redirect($this->redirectPath);
    }
}
