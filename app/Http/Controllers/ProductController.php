<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('product')->only(['store', 'update']);
    }

    public function index()
    {
        $products = Product::all();

        return view('products.index', ['products' => $products]);
    }


    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $product = new Product();
        $product->fill($request->all());
        $product->user()->associate(Auth::user());
        $product->save();

        return redirect(route('products.index'));
    }

    public function show(Product $product)
    {
        return view('products.view', ['product' => $product]);
    }

    public function edit(Product $product)
    {
        try {
            $this->authorize('edit', $product);
            return view('products.edit', ['product' => $product]);
        } catch (AuthorizationException $e) {
            return redirect()->back();
        }
    }

    public function update(Request $request, Product $product)
    {
        try {
            $this->authorize('edit', $product);
            $data = $request->all();
            $product->update($data);
            $product->save();
            return redirect(route('products.show', ['id'=>$product->id]));
        } catch (AuthorizationException $e) {
            return redirect()->back();
        }
    }

    public function destroy(Product $product)
    {
        try {
            $this->authorize('delete', $product);
            $product->delete();
            return redirect(route('products.index'));
        } catch (AuthorizationException $e) {
            return redirect()->back();
        }
    }
}
