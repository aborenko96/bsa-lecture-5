@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span>Product {{$product->name}}</span>
                        @can('edit', $product)
                            <a class="btn btn-primary mx-2" href="{{ route('products.edit', ['id' => $product->id]) }}">Edit</a>
                            <form method="POST" class="form-inline d-inline-block" action="{{route('products.destroy', ['id' => $product->id])}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        @endcan
                    </div>

                    <div class="card-body">
                        <p>Name: {{$product->name}}</p>
                        <p>Price: {{number_format($product->price, 2)}} $</p>
                        <p>Owner: {{$product->user->name}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
