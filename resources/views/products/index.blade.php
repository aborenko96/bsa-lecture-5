@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span>Products</span>
                        <a class="btn btn-success ml-2" href="{{ route('products.create') }}">Add</a>
                    </div>

                    <div class="card-body">
                        @foreach($products as $product)
                            <p>
                                <a href="{{route('products.show', ['id'=>$product->id])}}">{{$product->name}}: {{number_format($product->price, 2)}} $ ({{$product->user->name}})</a>
                            </p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
