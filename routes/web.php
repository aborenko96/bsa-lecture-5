<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\{
    Auth,
    Route
};

Route::get('/', "HomeController@index");

Route::prefix('/products')->group(function() {
    Route::get('/add', "ProductController@create")->name('products.create');
    Route::delete('/delete/{product}', "ProductController@destroy")->name('products.destroy');
});
Route::resource('products', 'ProductController')->except(['create', 'destroy']);

Auth::routes();

Route::get('/auth/facebook', 'SocialAuthController@redirectToProvider');
Route::get('/auth/facebook/callback', 'SocialAuthController@handleProviderCallback');
